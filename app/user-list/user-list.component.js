angular.
	module('userList').
	component('userList', {
		templateUrl: 'user-list/user-list.template.html',
		controller: ('userListController', [
			'$window',
			'$scope',
			'Users',
			function userListController($window, $scope, Users) {
				$scope.add = function(user) {
					Users.add(user);
					$scope.$emit('storageChange');
				}
				user = {};
				
				$scope.edit = function(editdata, id){
					if(angular.isUndefined(editdata)) return;

					var oldData = $scope.user(id);
					if(!oldData) return;

					var newData = {
						userId: oldData.userId,
						name: (editdata.name) ? editdata.name : oldData.name,
						surname: (editdata.surname) ? editdata.surname : oldData.surname,
						comment: (editdata.comment) ? editdata.comment : oldData.comment,
					}
					Users.edit(newData);
					$scope.$emit('storageChange');
				};
				$scope.delete = function(id){
					Users.delete(id);
					$scope.$emit('storageChange');
				}
				$scope.length = Users.length;
				$scope.users = Users.queryAll();
				$scope.user = function(id){
					return Users.queryOne(id);
				};
				$scope.editForm = true;
				$scope.$on('storageChange', function(){
					$scope.users = Users.queryAll();
				})
			}
		])
	});