angular.
	module('core.users').
	factory('Users', [
		'$window',
		function($window) {
				return {
					length : $window.localStorage.length,
					queryAll : function(){
						var userData = [];
						for(key in $window.localStorage) {
							if ($window.localStorage.getItem(key) !== null) {
								userData.push(JSON.parse($window.localStorage.getItem(key)))
							}
						}
						return userData;
					},
					findLatest: function(){
						if (this.length !== 0) {
							var latest = this.queryAll().sort(function(a, b){
								return b.userId - a.userId
							}).shift().userId;
						} else {
							latest = 0;
						}
						return latest;
					},
					queryOne: function(id){
						return JSON.parse($window.localStorage.getItem(id));
					},
					add: function(obj){
						obj.userId = this.findLatest()+1;
						$window.localStorage.setItem(obj.userId, JSON.stringify(obj));
						
					},
					edit: function(obj){
						console.log(obj)
						$window.localStorage.setItem(obj.userId, JSON.stringify(obj))
					},
					delete: function(id){
						$window.localStorage.removeItem(id);
					},
					clearAll: function(){
						$window.localStorage.clear();
					}
				}
		}
	])